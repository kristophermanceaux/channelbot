# Rasa Bot documentation
This was an experimentation created in my spare time. A lot more work went beyond this version, and the current version of this lives elsewhere

# Setup
- After cloning this repository, you will want to create a virtual environment in the root directory. Follow steps 1 and 2 located [here](https://rasa.com/docs/rasa/user-guide/installation/#create-a-virtual-environment-strongly-recommended)
- Once your virtual environment is created, activate it with `source venv/bin/activate`
- Note: just leave this virtual environment alone after this. You don't have any business inside of the virtual environment directory unless it's to activate or deactivate the virtual environment.
- Install all the dependencies from the `requirements.txt` into your virtual environment by using `pip install -r actions/requirements-actions.txt`
- To update this file in the future, make sure you are in your virtual environment and run `pip freeze > actions/requirements-actions.txt`
- For this project to log into your Rocket.Chat server as the appropriate BOT account and to make REST API calls you will need to create 2 files:
> 1) `credentials.yml`
> 2) `settings.yml`

#### credentials.yml
Copy and paste what's below and replace the URL, username, and password to the appropriate Rocket.Chat credentials. May not need this if you are just testing through the terminal and have no need to hook up to a Rocket.Chat server. I assume you wouldn't - I haven't tested it.
```
rest:
  # you don't need to provide anything here - this channel doesn't require any credentials

rasa:
  url: "http://localhost:5002/api"

rocketchat:
  user: "<username>"
  password: "<password>"
  server_url: "<server_URL>"
```


#### settings.yml
This file contains the required tokens and a base url for making API calls. May not need this if you are just testing through the terminal and have no need to hook up to a Rocket.Chat server. I assume you wouldn't - I haven't tested it.
```
access_token: <insert Rocket.Chat user access token>
user_id: <insert Rocket.Chat user ID token>
base_url: <insert your Rocket.Chat instance REST API base url>

```

# Run in terminal
- In your terminal in the project's root directory, you need to train the model with `rasa train`
- Then you can run the Bot server with `rasa shell` and in another terminal window, run `rasa run actions`. You can run both at the same time with `rasa shell & rasa run actions`, but I prefer to run them in separate windows
- With both running you can interact with your Bot where prompted for your input. Start by saying `create channel` or something similar. It should understand what your intentions are.
- Currently it I have the post API call commented out but the channel name will be verified to you in the terminal window.

# Interact with Channelbot through Rocket.Chat testing server
- This will require that you've setup a bot account on your Rocket.Chat server and enabled the ability for it to generate an access token. [Refer to the documentation](https://docs.rocket.chat/api/rest-api/personal-access-tokens)
- Once you are able to do that you will need to supply `credentials.yml` with the `username` and `password` of the BOT account and the URL of the Rocket.Chat home page. Also you will need to supply `settings.yml` with the `access token` and `user ID token` (If these are already generated, speak with someone to get these. You don't want to generate new tokens while someone is developing), along with the `base REST API URL` for your Rocket.Chat instance.
- You will need to setup an outgoing webhook within the Rocket.Chat admin settings. 
  - For this webhook, I am running my Rasa project on localhost, so I used `ngrok` to generate a URL for localhost:5005 by running `ngrok http 5005`. This URL will be used for the webhook in the URL section and follows this format: `http://<insert-ngrok-generated-forwarding-URL>/webhooks/rocketchat/webhook`
    - example:  `http://8d371007f12e.ngrok.io/webhooks/rocketchat/webhook`
  - Other required webhook settings:
    - Event trigger: Message Sent
    - Name: \<the bot's username\> (not rquired but I set this field)
    - Channel: @\<username> (this means for the event to trigger on direct messages to the BOT)
    - Trigger Words: * (This must be used at the beginning of every message to the Bot, example below)
    - Post as: \<username>
  - #### Then save!

With the webhook setup (double-check the URL) and after you have run `rasa train` and `rasa shell`, then run `rasa run actions` in another terminal window / tab, you can now chat with your bot by saying something like this in a direct message: 
```
*I would like to create a public channel
```
Then the conversation will be initiated and carry on from there. Just remember to always use your trigger at the beginning of every message to the Bot. I just used the * symbol because it was easy. It would be nice if we could trigger the webhook on every message to channelbot.
