# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/

from typing import Any, Text, Dict, List, Union
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import UserUtteranceReverted, SlotSet
from functools import reduce
from rasa_sdk.forms import FormAction
import requests
import json
import re
import yaml
import os

# TODO: LATER Rephrase form questions - be pedantic and privide examples to train the user
# TODO: LATER Figure out testing strategy.


def format_companies(company_name):
    prefix = 'p.'
    company_name_elements = re.sub('[*]', '', company_name).split()
    company_designation = reduce(lambda accumulator, element:
                                 accumulator + element[0],
                                 company_name_elements, ''
                                 )
    return prefix + company_designation.lower()


# TODO: look into refactoring get_channel_name much like I did with the required_slots function
# may change
def get_channel_name(is_sensitive, is_proprietary, slot_values, title_formatted, channel_part1):
    print('{}  {}  {}  {}  {}'.format(channel_part1, slot_values,
                                      is_proprietary, is_sensitive, title_formatted))
    if is_sensitive == 'yes' and is_proprietary == 'yes':
        channel_designations = '(' + slot_values['sensitive_info'] + \
            '/' + slot_values['company_names'] + ')'
        return '{}-{}-{}'.format(channel_part1,
                                 channel_designations,
                                 title_formatted)
    elif is_proprietary == 'yes' and is_sensitive == 'no':
        channel_designations = '(' + \
            slot_values['company_names'] + ')'
        return '{}-{}-{}'.format(channel_part1,
                                 channel_designations,
                                 title_formatted)
    elif is_proprietary == 'no' and is_sensitive == 'yes':
        channel_designations = '(' + \
            slot_values['sensitive_info'] + ')'
        return '{}-{}-{}'.format(channel_part1,
                                 channel_designations,
                                 title_formatted)
    else:  # this will catch all public and private channels that have no sensitive or proprietary information
        return '{}-{}'.format(channel_part1, title_formatted)


def get_account_credentials(settings):
    token = settings['access_token']
    user_id = settings['user_id']
    url = settings['base_url']
    headers = {
        'X-Auth-Token': token,
        'X-User-Id': user_id,
        'Content-type': 'application/json'
    }
    return url, headers


def set_channel_owner(settings, sender_id, room_id, access):
    url, headers = get_account_credentials(settings)
    endpoint = '/channels.addOwner' if access == 'public' \
        else '/groups.addOwner'
    data = {'roomId': room_id, 'userId': sender_id}
    response = requests.post(url + endpoint, headers=headers, json=data)
    # if response.status_code != 201:
    #     print('error making API call')
    return response.json()


def set_announcement(settings, room_id, announcement, access):
    url, headers = get_account_credentials(settings)
    endpoint = '/channels.setAnnouncement' if access == 'public' \
        else '/groups.setAnnouncement'
    data = {'roomId': room_id, 'announcement': announcement}
    response = requests.post(url + endpoint, headers=headers, json=data)
    return response.json()


def post_channel(settings, channel_name, username, access):
    url, headers = get_account_credentials(settings)
    endpoint = '/channels.create' if access == 'public' \
        else '/groups.create'
    data = {'name': channel_name, "members": [username]}
    response = requests.post(url + endpoint, headers=headers, json=data)
    return response.json()


def get_username(settings, sender_id):
    url, headers = get_account_credentials(settings)
    endpoint = '/users.info'
    response = requests.get(url + endpoint, headers=headers,
                            params={'userId': sender_id})
    return response.json()['user']['username']


def leave_channel(settings, room_id, access):
    url, headers = get_account_credentials(settings)
    endpoint = '/channels.leave' if access == 'public' \
        else '/groups.leave'
    data = {'roomId': room_id}
    response = requests.post(url + endpoint, headers=headers, json=data)
    return response.json()


def create_announcement(access, sensitive_info, company_names):
    public = 'This is a public channel. No SBU. No CUI. No PII. No proprietary information. No export-controlled information.'
    private_no_designation = 'This is a private channel. No sensitivity designations specified. No SBU. No CUI. No PII. No proprietary information. No export-controlled information.'
    prefix = "Information shared in this channel: "
    proprietary = [
        name.title() + ' Proprietary Information. ' for name in company_names.split(',')] if company_names != None else []

    itar = 'ITAR. '
    ear = 'EAR. '
    ear99 = 'EAR99. '
    pii = 'Personally Identifiable Information. '
    sbu = 'Sensitive But Unclassified Information. '
    ec = "Export-Controlled Information. "
    ec_no_mt = "Export-Controlled excluding any Missile Technology. "
    fouo = "For Official Use Only. "
    nci = "No Controlled Information. "
    cui = "Controlled Unclassified Information. "

    announcement_builder = public if access == 'public' \
        else private_no_designation if (access == 'private' and (sensitive_info == None and company_names == None)) \
        else prefix
    if sensitive_info != None:
        if 'itar' in sensitive_info:
            announcement_builder += itar
        if 'ear' in sensitive_info:
            announcement_builder += ear
        if 'ear99' in sensitive_info:
            announcement_builder += ear99
        if 'pii' in sensitive_info:
            announcement_builder += pii
        if 'sbu' in sensitive_info:
            announcement_builder += sbu
        if 'ec' in sensitive_info:
            announcement_builder += ec
        if 'ec.no.mt' in sensitive_info:
            announcement_builder += ec_no_mt
        if 'fouo' in sensitive_info:
            announcement_builder += fouo
        if 'nci' in sensitive_info:
            announcement_builder += nci
        if 'cui' in sensitive_info:
            announcement_builder += cui
    if company_names != None:
        if len(proprietary) > 0:
            announcement_builder += reduce(lambda accumulator, element:
                                           accumulator + element,
                                           proprietary, '')

    return announcement_builder


class ChannelForm(FormAction):
    def name(self):
        return "channel_form"

    @staticmethod
    def required_slots(tracker):
        access = tracker.get_slot('access')
        is_sensitive = tracker.get_slot('is_sensitive')
        is_proprietary = tracker.get_slot('is_proprietary')
        part1 = tracker.get_slot('part1')
        # TODO: figure the below logic without mutation
        slots_to_return = ['access', 'part1']

        if part1 == 'org':
            slots_to_return += ['org_name']
        elif part1 == "program":
            slots_to_return += ['program_name']

        if access == 'private':
            slots_to_return += ['is_proprietary']
        if is_proprietary == 'yes':
            slots_to_return += ['company_names']
        if access == 'private':
            slots_to_return += ['is_sensitive']
        if is_sensitive == 'yes':
            slots_to_return += ['sensitive_info']

        slots_to_return += ['title', 'is_okay']

        return slots_to_return

    def submit(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict]:

        stream = open('actions/settings.yml', 'r')
        settings = yaml.load(stream, Loader=yaml.BaseLoader)
        access = tracker.get_slot('access')
        sensitive_info = tracker.get_slot('sensitive_info')
        company_names_raw = tracker.get_slot('company_names_raw')

        if tracker.get_slot('is_okay') == 'yes':

            channel_name = tracker.get_slot('channel_name')

            ###  Uncomment below to POST new channel ###
            most_recent_state = tracker.current_state()
            sender_id = most_recent_state['sender_id'][:17]

            username = get_username(settings, sender_id)

            channel_post_response = post_channel(
                settings, channel_name, username, access)

            if channel_post_response['success'] == True:
                room_id = channel_post_response['channel']['_id'] if access == 'public' \
                    else channel_post_response['group']['_id']

                add_owner_response = set_channel_owner(
                    settings, sender_id, room_id, access)

                announcement_post_response = set_announcement(
                    settings, room_id, create_announcement(access, sensitive_info, company_names_raw), access)

                leave_response = leave_channel(
                    settings, room_id, access)

                dispatcher.utter_message("Channel created")
            elif channel_post_response['success'] == False \
                    and (channel_post_response['errorType'] == 'error-duplicate-channel-name'
                         or channel_post_response['errorType'] == 'error-duplicate-group-name'):
                dispatcher.utter_message(
                    'Channel creation failed.\n Channel already exists.\nTo try again, please re-initiate the conversation by saying \"create channel\"')
                self.deactivate()
            else:
                dispatcher.utter_message(
                    'Channel creation failed.\nTo try again, please re-initiate the conversation by saying \"create channel\"')
                self.deactivate()

        return []

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict[Text, Any]]]]:
        """A dictionary to map required slots to
        - an extracted entity
        - intent: value pairs
        - a whole message
        or a list of them, where a first match will be picked"""
        return {
            "title": self.from_text(),
            "org_name": self.from_text(),
            "is_sensitive": [
                self.from_intent(intent='affirm', value='yes'),
                self.from_intent(intent='deny', value='no')
            ],
            "is_okay": [
                self.from_intent(intent='affirm', value='yes'),
                self.from_intent(intent='deny', value='no')
            ],
            "is_proprietary": [
                self.from_intent(intent='affirm', value='yes'),
                self.from_intent(intent='deny', value='no')
            ],
            "access": [
                self.from_intent(intent='public_channel', value='public'),
                self.from_intent(intent='private_channel', value='private')
            ]
        }

    def validate_access(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validate access value."""
        return {'access': value}

    def validate_part1(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validate part1 value."""

        part1 = re.sub('[*]', '', value).lower()
        data = ''
        print(part1)
        with open(os.path.abspath("data/test/lookup_tables/part1.txt"), "r") as file:
            data = file.read()
            print('data {}'.format(data))
        if part1 not in data:
            return {'part1': None}
        print(part1)
        return {'part1': part1}

    def validate_sensitive_info(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validate sensitive_info value."""
        sensitivity = re.sub('[*]', '', value)

        placeholder = tracker.get_slot('sensitive_info_placeholder') \
            if tracker.get_slot('sensitive_info_placeholder') != None \
            else ''
        if sensitivity in placeholder:
            return {'sensitive_info': None,
                    'sensitive_info_placeholder': placeholder}
        if sensitivity != 'done with sensitivity designations':
            placeholder = placeholder + sensitivity + '/'  # take slash off very end
            return {'sensitive_info': None,
                    'sensitive_info_placeholder': placeholder}
        if placeholder == '':
            return {
                'sensitive_info': placeholder,
                'is_sensitive': 'no'
            }
        return {'sensitive_info': placeholder[:-1]}

    def validate_company_names(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validate company_names value."""

        cleaned_value = re.sub('[*]', '', value)

        placeholder = tracker.get_slot('company_names_placeholder') \
            if tracker.get_slot('company_names_placeholder') != None \
            else ''
        raw_list_company_names = tracker.get_slot('company_names_raw') \
            if tracker.get_slot('company_names_raw') != None \
            else ''
        company = cleaned_value.split(',')
        if cleaned_value != 'done with company names' and len(company) == 2:
            name = company[0]
            designation = company[1]
            if designation in placeholder:
                return {'company_names': None,
                        'company_names_placeholder': placeholder,
                        'company_names_raw': raw_list_company_names}
            placeholder = placeholder + designation + \
                '/'  # take slash off very end (-1)
            raw_list_company_names = raw_list_company_names + \
                name + ', '  # take space and comma off very end (-2)
            return {'company_names': None,
                    'company_names_placeholder': placeholder,
                    'company_names_raw': raw_list_company_names}
        print('cleaned_value:  {}'.format(cleaned_value))
        print('raw_list_company_names:  {}'.format(raw_list_company_names))
        print('placeholder for company names:  {}'.format(placeholder))
        if placeholder == '':
            print('went straight for done for company names')
            return {
                'company_names': placeholder,
                'company_names_raw': None,
                'is_proprietary': 'no'
            }
        return {'company_names': placeholder[:-1], 'company_names_raw': raw_list_company_names[:-2]}

    def validate_title(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validate access value."""
        slot_values = tracker.current_slot_values()
        channel_part1 = slot_values['org_name'] if slot_values['part1'] == 'org' \
            else slot_values['program_name'] if slot_values['part1'] == 'program' \
            else slot_values['part1']
        print('in validate title after setting channel_part1: {}'.format(channel_part1))
        is_sensitive = slot_values['is_sensitive']
        is_proprietary = slot_values['is_proprietary']

        title = value
        split_title = title.split()
        cleaned_split_title = list(map(lambda element:
                                       re.sub('[^A-Za-z0-9&_]', '', element),
                                       split_title))
        cleaned_filtered_title = [
            element for element in cleaned_split_title if element != '']
        title_formatted = reduce(lambda title, current_title_component:
                                 title + current_title_component + '_',
                                 cleaned_filtered_title,  '')[:-1].lower()
        print('in validate title: {}'.format(channel_part1))
        channel_name = get_channel_name(
            is_sensitive, is_proprietary, slot_values, title_formatted, channel_part1)

        dispatcher.utter_message(channel_name)
        return {
            'title': title_formatted,
            'channel_name': channel_name
        }

    def validate_org_name(self,
                          value: Text,
                          dispatcher: CollectingDispatcher,
                          tracker: Tracker,
                          domain: Dict[Text, Any],
                          ) -> Dict[Text, Any]:
        name_split = re.sub('[,-/.*]', ' ', value)
        name_split = name_split.split(' ')
        cleaned_split = [part for part in name_split if part != '']
        print('name_split {}'.format(cleaned_split))
        cleaned_name_split = list(map(lambda element:
                                      re.sub('[^A-Za-z0-9&_]', '', element),
                                      cleaned_split))
        org_name = reduce(lambda title, current_title_component:
                          title + current_title_component + '_',
                          cleaned_name_split,  '')[:-1].lower()
        return {'org_name': org_name}

    def validate_program_name(self,
                              value: Text,
                              dispatcher: CollectingDispatcher,
                              tracker: Tracker,
                              domain: Dict[Text, Any],
                              ) -> Dict[Text, Any]:
        name_split = value.split(',')

        cleaned_name_split = list(map(lambda element:
                                      re.sub('[^A-Za-z0-9&_]', '', element),
                                      name_split))
        program_name = reduce(lambda title, current_title_component:
                              title + current_title_component + '_',
                              cleaned_name_split,  '')[:-1].lower()

        return {'program_name': program_name}

    def validate_is_okay(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validate is_okay value."""
        if value == 'no':
            dispatcher.utter_message(
                'Form creation cancelled.\nTo try again, please re-initiate the conversation by saying \"create channel\"')
            self.deactivate()
        return {'is_okay': value}
