<!-- ## intent:greet
- Hi
- Hello -->

## lookup:part1
data/test/lookup_tables/part1.txt  
## lookup:sensitive_info
data/test/lookup_tables/sensitive_info.txt
## lookup:company_names
data/test/lookup_tables/company_names.txt
## lookup:program_name
data/test/lookup_tables/program_name.txt

## intent:inform_category
- [topic](part1)
- [group](part1)
- [team](part1)
- [event](part1)
- [org](part1)
- [program](part1)

## intent:inform_sensitive_info
- [sbu](sensitive_info)
- [ec.no.mt](sensitive_info)
- [ec](sensitive_info)
- [itar](sensitive_info)
- [ear](sensitive_info)
- [ear99](sensitive_info)
- [fouo](sensitive_info)
- [nci](sensitive_info)
- [cui](sensitive_info)
- [pii](sensitive_info)
- [done with sensitivity designations](sensitive_info)

## intent:inform_company_names
- [SpaceX,p.sx](company_names)
- [Blue Origin,p.blo](company_names)
- [Northrop Grumman,p.ng](company_names)
- [Sierra Nevada,p.sn](company_names)
- [Boeing,p.boe](company_names)
- [Maxar,p.mx](company_names)
- [Dynetics,p.dyn](company_names)
- [National Team,p.nt](company_names)
- [Lockheed Martin,p.lm](company_names)
- [Intuitive Machines,p.im](company_names)
- [Masten Space Systems,p.mss](company_names)
- [done with company names](company_names)

## intent:initiate_channel
- channel
- create channel
- create
- I would like to create a channel
- I need a channel
- I can has channel
- I want a channel
- A channel is what I need

## intent:inform_program_name
- [comm_crew](program_name)
- [comm_resupply](program_name)
- [iss](program_name)
- [gateway](program_name)
- [hls](program_name)
- [artemis](program_name)
- [sls](program_name)
- [orion](program_name)
- [clps](program_name)

## intent:public_channel
- it's [public](access)
- [public](access) channel
- [public](access)

## intent:private_channel
- it's [private](access)
- [private](access) channel
- [private](access)

## intent:affirm
- yes
- yeah
- yup

## intent:deny
- no
- nope
- negatory

## intent:quit_form
- exit the form
- I want to stop this form
- quit form
- stop
- quit
- exit