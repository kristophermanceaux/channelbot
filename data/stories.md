## happy channel form
* initiate_channel
  - channel_form
  - form{"name": "channel_form"}
  - form{"name": null}
  - action_restart

## happy channel form + interrupt + stop
* initiate_channel
  - channel_form
  - form{"name": "channel_form"}
* quit_form
  - utter_ask_to_continue
* deny
  - action_deactivate_form
  - form{"name": null}
  - action_restart

## happy channel form + interrupt + continue
* initiate_channel
  - channel_form
  - form{"name": "channel_form"}
* quit_form
  - utter_ask_to_continue
* affirm
  - channel_form
  - action_restart
