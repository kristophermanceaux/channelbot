if [ -d "$models" ]; then
  rm -rf models
fi
docker run -v $(pwd):/app rasa/rasa:1.10.3-full train
docker build -t channelbot .
docker-compose up -d
